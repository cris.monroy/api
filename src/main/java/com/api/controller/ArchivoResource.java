package com.api.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.api.file.Archivo;
import com.api.file.TipoArchivo;
import com.api.file.repositories.ArchivoRepository;


@RestController
public class ArchivoResource {

	@Autowired
	private ArchivoRepository ArchivoRepository;
	
	@GetMapping("/file/pruebas")
	public List<Archivo> retrieveAllArchivo() {
		return ArchivoRepository.findAll();
	}
}
