package com.api.file;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "TipoAlmacenamiento", schema="db_files.FILES")
public class TipoAlmacenamiento {
		
	 @Id
	 @GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	 @GeneratedValue(generator = "generator")
	 @Column(name = "PK_IdTipoAlmacenamiento" , columnDefinition="uniqueidentifier")
	    private String PK_IdTipoAlmacenamiento;
	    private String Descripcion;
	    
	    public TipoAlmacenamiento(){
	    	
	    }
	    
	    public TipoAlmacenamiento(String pK_IdTipoAlmacenamiento, String descripcion){
	    	this.PK_IdTipoAlmacenamiento=pK_IdTipoAlmacenamiento;
	    	this.Descripcion=descripcion;
	    	
	    	
	    }

	   		
		

		public String getPK_IdTipoAlmacenamiento() {
			return PK_IdTipoAlmacenamiento;
		}

		public void setPK_IdTipoAlmacenamiento(String pK_IdTipoAlmacenamiento) {
			PK_IdTipoAlmacenamiento = pK_IdTipoAlmacenamiento;
		}

		public String getDescripcion() {
			return Descripcion;
		}

		public void setDescripcion(String descripcion) {
			Descripcion = descripcion;
		}

		@Override
	    public String toString() {
	        return "PruebaFile{" +
	                ", PK_IdTipoAlmacenamiento='" + PK_IdTipoAlmacenamiento + '\'' +
	                ", Descripcion=" + Descripcion +
	                '}';
	    }
	    
}
