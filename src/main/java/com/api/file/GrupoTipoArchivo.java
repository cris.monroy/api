package com.api.file;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "GrupoTipoArchivo", schema="db_files.FILES")
public class GrupoTipoArchivo {
		
	 @Id
	 @GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	 @GeneratedValue(generator = "generator")
	 @Column(name = "PK_IdGrupoTipoArchivo" , columnDefinition="uniqueidentifier")
	    private String PK_IdGrupoTipoArchivo;
	    private String Nombre;
	    private String Descripcion;
	    private boolean CT_LIVE;
	    private Date CT_CREATE;
	    private Date CT_UPDATE;
	    
	    public GrupoTipoArchivo(){
	    	
	    }
	    
	    public GrupoTipoArchivo(String pK_IdGrupoTipoArchivo, String nombre,
	    		String descripcion, boolean cT_LIVE, Date cT_CREATE,Date cT_UPDATE ){
	    	this.PK_IdGrupoTipoArchivo=pK_IdGrupoTipoArchivo;
	    	this.Nombre=nombre;
	    	this.Descripcion=descripcion;
	    	this.CT_LIVE=cT_LIVE;
	    	this.CT_CREATE=cT_CREATE;
	    	this.CT_UPDATE=cT_UPDATE;
	    	
	    	
	    }

	
		

		public String getPK_IdGrupoTipoArchivo() {
			return PK_IdGrupoTipoArchivo;
		}

		public void setPK_IdGrupoTipoArchivo(String pK_IdGrupoTipoArchivo) {
			PK_IdGrupoTipoArchivo = pK_IdGrupoTipoArchivo;
		}

		public String getNombre() {
			return Nombre;
		}

		public void setNombre(String nombre) {
			Nombre = nombre;
		}

		public String getDescripcion() {
			return Descripcion;
		}

		public void setDescripcion(String descripcion) {
			Descripcion = descripcion;
		}

		public boolean isCT_LIVE() {
			return CT_LIVE;
		}

		public void setCT_LIVE(boolean cT_LIVE) {
			CT_LIVE = cT_LIVE;
		}

		public Date getCT_CREATE() {
			return CT_CREATE;
		}

		public void setCT_CREATE(Date cT_CREATE) {
			CT_CREATE = cT_CREATE;
		}

		public Date getCT_UPDATE() {
			return CT_UPDATE;
		}

		public void setCT_UPDATE(Date cT_UPDATE) {
			CT_UPDATE = cT_UPDATE;
		}

		@Override
	    public String toString() {
	        return "PruebaFile{" +
	                ", PK_IdGrupoTipoArchivo='" + PK_IdGrupoTipoArchivo + '\'' +
	                ", Nombre='" + Nombre + '\'' +
	                ", Descripcion='" + Descripcion + '\'' +
	                ", CT_LIVE='" + CT_LIVE + '\'' +
	                ", CT_CREATE='" + CT_CREATE + '\'' +
	                ", CT_UPDATE=" + CT_UPDATE +
	                '}';
	    }
	    
		
}
