package com.api.file;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Archivo", schema="db_files.FILES")
public class Archivo {
		
	 @Id
	 @GenericGenerator(name = "generator", strategy = "guid", parameters = {})
	 @GeneratedValue(generator = "generator")
	 @Column(name = "PK_IdArchivo" , columnDefinition="uniqueidentifier")
	    private String PK_IdArchivo;
	 @Column(name = "FK_IdMd5File_FILES" , columnDefinition="uniqueidentifier")
	    private String FK_IdMd5File_FILES;
	 @Column(name = "FK_IdTipoArchivo_FILES" , columnDefinition="uniqueidentifier")
	    private String FK_IdTipoArchivo_FILES;
	 private String FileName;
	 private String MetaData;
	 private boolean CT_LIVE;
	 private Date CT_CREATE;
	 private Date CT_UPDATE;
	 
	 
	 
	    public Archivo(){
	    	
	    }
	    
	    public Archivo(String pK_IdArchivo, String fK_IdMd5File_FILES, String fK_IdTipoArchivo_FILES
	    		,String fileName,String metaData,boolean cT_LIVE,Date cT_CREATE,Date cT_UPDATE){
	    	this.PK_IdArchivo=pK_IdArchivo;
	    	this.FK_IdMd5File_FILES=fK_IdMd5File_FILES;
	    	this.FK_IdTipoArchivo_FILES=fK_IdTipoArchivo_FILES;
	       	this.FileName=fileName;
	    	this.MetaData=metaData;
	    	this.CT_LIVE=cT_LIVE;
	    	this.CT_CREATE=cT_CREATE;
	    	this.CT_UPDATE=cT_UPDATE;
	    }

	 

		public String getPK_IdArchivo() {
			return PK_IdArchivo;
		}

		public void setPK_IdArchivo(String pK_IdArchivo) {
			PK_IdArchivo = pK_IdArchivo;
		}

		public String getFK_IdMd5File_FILES() {
			return FK_IdMd5File_FILES;
		}

		public void setFK_IdMd5File_FILES(String fK_IdMd5File_FILES) {
			FK_IdMd5File_FILES = fK_IdMd5File_FILES;
		}

		public String getFK_IdTipoArchivo_FILES() {
			return FK_IdTipoArchivo_FILES;
		}

		public void setFK_IdTipoArchivo_FILES(String fK_IdTipoArchivo_FILES) {
			FK_IdTipoArchivo_FILES = fK_IdTipoArchivo_FILES;
		}

		public String getFileName() {
			return FileName;
		}

		public void setFileName(String fileName) {
			FileName = fileName;
		}

		public String getMetaData() {
			return MetaData;
		}

		public void setMetaData(String metaData) {
			MetaData = metaData;
		}

		public boolean isCT_LIVE() {
			return CT_LIVE;
		}

		public void setCT_LIVE(boolean cT_LIVE) {
			CT_LIVE = cT_LIVE;
		}

		public Date getCT_CREATE() {
			return CT_CREATE;
		}

		public void setCT_CREATE(Date cT_CREATE) {
			CT_CREATE = cT_CREATE;
		}

		public Date getCT_UPDATE() {
			return CT_UPDATE;
		}

		public void setCT_UPDATE(Date cT_UPDATE) {
			CT_UPDATE = cT_UPDATE;
		}

		@Override
	    public String toString() {
	        return "Archivo{" +
	                ", PK_IdArchivo='" + PK_IdArchivo + '\'' +
	                ", FK_IdMd5File_FILES='" + FK_IdMd5File_FILES + '\'' +
	                ", FK_IdTipoArchivo_FILES='" + FK_IdTipoArchivo_FILES + '\'' +
	                ", FileName='" + FileName + '\'' +
	                ", MetaData='" + MetaData + '\'' +
	                ", CT_LIVE='" + CT_LIVE + '\'' +
	                ", CT_UPDATE='" + CT_UPDATE + '\'' +
	                ", FK_IdMd5File_FILES=" + FK_IdMd5File_FILES +
	                '}';
	    }
		
}
